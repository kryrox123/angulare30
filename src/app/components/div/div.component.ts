import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-div',
  templateUrl: './div.component.html',
  styleUrls: ['./div.component.css']
})
export class DivComponent implements OnInit {

  tamano: number = 30
  text: string = '';
  textErr: string = 'Llegó al limite'
  constructor() { }

  ngOnInit(): void {

  }

  aumentar(){
    this.tamano = this.tamano + 5
    if (this.tamano === 500){
      this.text = this.textErr
    }
  }

  disminuir(){
    this.tamano = this.tamano - 5;
    if(this.tamano === 5){
      this.text = this.textErr
    }
  }
}
